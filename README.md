# LEGO Price Scrapper

### Supported sites
* [kubik-shop.by](kubik-shop.by)
* [mirkubikov.by](http://mirkubikov.by)
* [brickclub.by](https://brickclub.by)

### Running
Goes through sites to find LEGO sets with the lowest prices

#### Searching *LEGO Technic 42100* by default
> python src/main.py
#### LEGO set and theme can be set
> python src/main.py set=51515 theme=Mindstorms
### Output format
| name                                     |   price | link                                                                       | description                            | site                  |
|:-----------------------------------------|--------:|:---------------------------------------------------------------------------|:---------------------------------------|:----------------------|
| 51515 Lego Mindstorms Робот-изобретатель |  786.9  | https://kubik-shop.by/lego-mindstorms/51515-lego-mindstorms-robot-inventor |                                        | https://kubik-shop.by |
| Robot Inventor                           |  799    | https://brickclub.by/legocatalog/mindstorms/51515                          | Shop with lowest price detected: Egogo | https://brickclub.by  |
| 51515 Робот-изобретатель Lego Mindstorms |  798.99 | http://mirkubikov.by/catalog/lego/mindstorms/556/                          | availability count=Достаточно          | http://mirkubikov.by  |