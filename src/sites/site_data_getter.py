from abc import abstractmethod


class SiteDataGetter:

    @abstractmethod
    def get_goods(self, theme_name, set_code):
        pass

    def get_one(self, theme_name, set_code):
        """
        :param theme_name: Ex. Mindstorms
        :param set_code: Ex. 51515
        :return: item of Product model if the only item found, throws Exception
        """
        goods = self.get_goods(theme_name, set_code)
        if len(goods) == 0:
            return None
        if len(goods) == 1:
            return goods[0]
        raise Exception("More than 1 element found")
