from sites.site_data_getter import SiteDataGetter
from sites.model import Product
from sites.util import xpath_elements


class MirKubikovByScrapper(SiteDataGetter):
    SITE = "http://mirkubikov.by"
    PRODUCTS_XPATH = '//div[@class="item_info main_item_wrapper TYPE_1"]'

    def get_goods(self, theme_name, set_code):
        request_str = "+".join([theme_name, set_code])
        search_url = ("http://mirkubikov.by/catalog/?q=" + request_str)

        elements = xpath_elements(search_url, self.PRODUCTS_XPATH)

        goods = []
        for element in elements:
            availability = element.xpath('div[@class="item-stock"]')[0].xpath("span[@class='value']")[0].text
            if availability in ['Нет в наличии']:  # ['Нет в наличии, Достаточно, Мало]
                continue
            name_str = element.xpath('div[@class="item-title"]/a/span')[0].text
            link_str = self.SITE + element.xpath('div[@class="item-title"]/a')[0].attrib['href']
            price_str = element.xpath('div[contains(@class,"price")]'
                                      '/div[@class="price"]'
                                      '/span[@class="values_wrapper"]'
                                      '/span[@class="price_value"]')[0].text
            price_val = float(price_str.replace(" ", ""))
            description = "availability count=" + availability
            goods.append(Product(name_str, price_val, link_str, self.SITE, description=description))

        return goods
