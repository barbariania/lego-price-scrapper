class Product:
    def __init__(self, name, price, link, site, description=None):
        self.name = name
        self.price = price
        self.link = link
        self.description = description
        self.site = site
