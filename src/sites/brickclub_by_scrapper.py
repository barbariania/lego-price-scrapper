from sites.site_data_getter import SiteDataGetter
from sites.model import Product
from sites.util import xpath_elements


class BrickClubByScrapper(SiteDataGetter):
    SITE = "https://brickclub.by"
    PRODUCTS_XPATH = '//*[@id="content"]/div[1]'

    def get_goods(self, theme_name, set_code):
        search_url = "https://brickclub.by/?s=" + set_code
        elements = xpath_elements(search_url, self.PRODUCTS_XPATH)

        goods = []
        for element in elements:
            product_name = element.xpath("div/div/div[3]/a")[0].text
            price = float(element.xpath('div/div/div[4]/span[2]')[0].text)
            link = self.SITE + element.xpath('div/div/div[3]/a')[0].attrib['href']
            description = "Shop with lowest price detected: " + element.xpath('div/div/div[4]/span[1]')[0].text
            goods.append(Product(product_name, price, link, self.SITE, description=description))
        return goods
