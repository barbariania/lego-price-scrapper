import re

from sites.site_data_getter import SiteDataGetter
from sites.model import Product
from sites.util import xpath_elements


class KubikShopByScrapper(SiteDataGetter):
    SITE = "https://kubik-shop.by"
    goods_list = []

    def __get_search_results__(self, theme_name, set_code):
        search_request = "%20".join([theme_name, set_code])
        link = "https://kubik-shop.by/index.php?route=product/search&search=" + search_request + "&description=true"

        products_xpath = '//*[@id="content"]/div[3]/div[.//div[contains(@class, "product-thumb")]]' \
                         '/div//div[contains(@class, "caption")]'
        # results = soup.find(posixpath = products_xpath)
        elements = xpath_elements(link, products_xpath)

        goods = []
        for element in elements:
            name_str = element.xpath("a")[0].text
            link_str = element.xpath("a")[0].attrib['href']
            price_str = element.xpath("p[@class='price']/span[@class='price-new']")[0].text
            price_float = float(re.findall("([0-9]+[,.]+[0-9]+)", price_str)[0])
            goods.append(Product(name=name_str, price=price_float, link=link_str, site=self.SITE))
        return goods

    def get_goods(self, theme_name, set_code):
        return self.goods_list if self.goods_list else self.__get_search_results__(theme_name, set_code)
