"""
Utilities for getting page content
"""
import urllib.request
import requests
from lxml import etree
from bs4 import BeautifulSoup


def xpath_elements(search_url, products_xpath):
    soup = BeautifulSoup(get_page_content(search_url), 'html.parser')
    site_soap_tree = etree.HTML(str(soup))
    elements = site_soap_tree.xpath(products_xpath)
    return elements


def get_page_content(search_url):
    # return __page_content_requests__(search_url)
    return __page_content_urllib__(search_url)


def __page_content_requests__(search_url):
    headers = {
        'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36'}
    page = requests.get(search_url, headers=headers)
    if page.status_code != 200:
        raise Exception("Response code is not OK but ", page.status_code, page.content[0:200])
    return page.content


def __page_content_urllib__(search_url):
    with urllib.request.urlopen(search_url) as search_result:
        search_result_text_bytes = search_result.read()
        unicode_text = str(search_result_text_bytes, 'utf-8')
        if search_result.code != 200:
            raise Exception("Response code is not OK but ", search_result.code, unicode_text[0:200])
        return unicode_text
