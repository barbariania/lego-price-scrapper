import sys

from tabulate import tabulate

from sites import kubik_shop_by_scrapper, mir_kubikov_by_scrapper, brickclub_by_scrapper


def find_all_items_and_add_to_goods(site_data_getter_list, theme_name, set_code):
    for site_data_getter in site_data_getter_list:
        products = site_data_getter.get_goods(theme_name, set_code)
        for product in products:
            goods.append(vars(product).values())


def find_item_and_add_to_goods(sdg, request_str):
    product = sdg.get_one(request_str)
    if product:
        goods.append(vars(product).values())


if __name__ == '__main__':
    LEGO_THEME_NAME = "TECHNIC"
    LEGO_SET_CODE = "42100"

    for arg in sys.argv:
        if "theme" in arg:
            LEGO_THEME_NAME = arg.replace("theme=", "")
            continue
        if "set" in arg:
            LEGO_SET_CODE = arg.replace("set=", "")
            continue

    goods = []
    site_data_getters = [
        kubik_shop_by_scrapper.KubikShopByScrapper(),
        brickclub_by_scrapper.BrickClubByScrapper(),
        mir_kubikov_by_scrapper.MirKubikovByScrapper()
    ]

    sites = [sdg.SITE for sdg in site_data_getters]
    print("Searching for LEGO theme=" + LEGO_THEME_NAME + " set=" + LEGO_SET_CODE + " in sites: " + str(sites))

    find_all_items_and_add_to_goods(site_data_getters, LEGO_THEME_NAME, LEGO_SET_CODE)

    print(tabulate(goods, headers=['name', 'price', 'link', 'description', 'site'], tablefmt='pipe'))
